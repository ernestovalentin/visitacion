<?php
require_once "conexion.php";
class CRUD extends Conexion{

    //CRUDIngresoPlataformaModel permite el acceso a la plataforma. (Inicio de sesion)
    public function CRUDIngresoPlataformaModel($datosModel, $tabla){
      $stmt = Conexion::conectar()->prepare("SELECT `correo`, `contra`, `nombre` FROM $tabla WHERE `correo` = :correo");
      $stmt ->bindParam(":correo", $datosModel["usuario"], PDO::PARAM_STR);
      $stmt -> execute();
      echo "Consultando";
      return $stmt->fetch();
      $stmt -> close();
    }

  //CRUDRegistroCasetasModel permite registrar una nueva caseta en datos de Sian Ka'an
  public function CRUDRegistroCasetasModel($datosModel, $tabla){
    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(`nombre`, `estacion`) VALUES (:nombre,:estacion)");
    $stmt -> bindParam(":nombre",$datosModel['nombre'], PDO::PARAM_STR);
    $stmt -> bindParam(":estacion",$datosModel['estacion'], PDO::PARAM_STR);
    if ($stmt->execute()) {
      return true;
    }else {
      return false;
    }
    $stmt -> close();
  }

  //CRUDRegistroGuardaparquesModel permite registrar un nuevo guardaparque en la vista datos de sian Kaan
  public function CRUDRegistroGuardaparquesModel($datosModel, $tabla){
    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(`nombre`, `apellido`, `clave`) VALUES (:nombre,:apellido,:clave)");
    $stmt -> bindParam(":nombre",$datosModel['nombre'], PDO::PARAM_STR);
    $stmt -> bindParam(":apellido", $datosModel['apellido'], PDO::PARAM_STR);
    $stmt -> bindParam(":clave", $datosModel['clave'], PDO::PARAM_STR);
    if ($stmt -> execute()) {
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  //CRUDRegistroVisitacionModel permite realizar el registro de Visitacion
  public function CRUDRegistroVisitacionModel($datosModel, $tabla){
    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(`fecha`, `idcasetas`, `comentarios`) VALUES (:fecha,:idcasetas,:comentarios)");
    $stmt -> bindParam(":fecha", $datosModel['fecha'], PDO::PARAM_STR);
    $stmt -> bindParam(":idcasetas", $datosModel['caseta'], PDO::PARAM_STR);
    $stmt -> bindParam(":comentarios", $datosModel['comentarios'], PDO::PARAM_STR);
    if ($stmt -> execute()) {
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  //CRUDRegistroVisitantesModel permite registrar el numero de visitantes con un ID especifico de Visitacion
  public function CRUDRegistroVisitantesModel($tabla, $idVisitacion, $visitantes, $idnacionalidades){
    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(`idvisitacion`, `visitantes`, `idnacionalidades`) VALUES (:idvisitacion,:visitantes,:idnacionalidades)");
    $stmt->bindParam(":idvisitacion", $idVisitacion, PDO::PARAM_STR);
    $stmt->bindParam(":visitantes", $visitantes ,PDO::PARAM_STR);
    $stmt->bindParam(":idnacionalidades", $idnacionalidades, PDO::PARAM_STR);
    if ($stmt->execute()) {
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  //CRUDRegistroVisitacionGuardaparquesModel permite registrar los guardaparques de Visitacion
  public function CRUDRegistroVisitacionGuardaparquesModel($tabla, $idvisitacion, $idguardaparque ){
    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(`idvisitacion`, `idguardaparques`) VALUES (:idvisitacion,:idguardaparque)");
    $stmt->bindParam(":idvisitacion",$idvisitacion, PDO::PARAM_STR);
    $stmt->bindParam(":idguardaparque",$idguardaparque, PDO::PARAM_STR);
    if ($stmt->execute()) {
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  //CRUDRegistroVehiculosModel permite registrar los vehiculos de un registro de Visitacion
  public function CRUDRegistroVehiculosModel($tabla, $idvisitacion, $datosModel){
    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(`idvisitacion`, `num_bici`, `num_moto`, `num_auto`, `num_carga`) VALUES ($idvisitacion, :bici,:moto,:auto,:carga)");
    $stmt -> bindParam(":bici", $datosModel['bici'], PDO::PARAM_STR);
    $stmt -> bindParam(":moto", $datosModel['moto'], PDO::PARAM_STR);
    $stmt -> bindParam(":auto", $datosModel['auto'], PDO::PARAM_STR);
    $stmt -> bindParam(":carga", $datosModel['carga'], PDO::PARAM_STR);
    if ($stmt->execute()) {
      return true;
    }else {
      return false;
    }
    $stmt -> close();
  }

  //CRUDVistaVerVisitacionModel permite consultar los ID de visitacion de entre un rango de fechas
  public function CRUDVistaVerVisitacionModel($datosModel, $tabla){
    $stmt = Conexion::conectar()->prepare("SELECT `idvisitacion`, `fecha`, `caseta`, `visitantes`, `nacionalidad` FROM $tabla WHERE `fecha` BETWEEN :inicio AND :fin");
    $stmt->bindParam(":inicio", $datosModel['inicio'], PDO::PARAM_STR);
    $stmt->bindParam(":fin", $datosModel['fin'], PDO::PARAM_STR);
    if ($stmt->execute()) {
      return $stmt->fetchAll();
      $stmt->close();
    }else {
      echo "Error en consulta de fechas rango";
    }
  }

  public function CRUDVistaVisitacionIdModel($id){
    $stmt = Conexion::conectar()->prepare("SELECT `idvisitacion`, `idregistro`, `fecha`, `idcaseta`, `caseta`, `visitantes`, `nacionalidad` FROM `vervisitacion` WHERE `idvisitacion` = $id");
    if ($stmt->execute()) {
      return $stmt->fetchAll();
      $stmt->close();
    }else{
      echo "Error en la consulta";
    }
  }

  //CRUDVistaVisitacionYearModel permite ver los años de los datos de la tabla visitacion
  public function CRUDVistaVisitacionYearModel(){
    $stmt=Conexion::conectar()->prepare("SELECT EXTRACT(year from `fecha`) as anio from `visitacion` group by EXTRACT(year from `fecha`)");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
  }

  //CRUDVistaVisitacionYearMinMaxModel permite obtener el año minimo y maximo de la tabla de visitacion.
  public function CRUDVistaVisitacionYearMinMaxModel(){
    $stmt=Conexion::conectar()->prepare("SELECT MIN(EXTRACT(year from `fecha`)) as amin, MAX(EXTRACT(year from `fecha`)) as amax from `visitacion`");
    if ($stmt->execute()) {
      return $stmt->fetchAll();
    }
    $stmt -> close();
  }

  //CRUDVistaVisitacionMountModel
  public function CRUDVistaVisitacionMonthModel(){
    $stmt=Conexion::conectar()->prepare("SELECT EXTRACT(MONTH from `fecha`) as mes from `visitacion` group by EXTRACT(MONTH from `fecha`)");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
  }

  //CRUDVistaUsuariosModel permite visualizar los usuarios utilizando su ID (Configuracion de cuentas de usuario)
  public function CRUDVistaUsuariosModel($tabla, $id){
    $stmt = Conexion::conectar()->prepare("SELECT `nombre`, `apeliido`, `correo`, `contra` FROM `usuarios` WHERE `idusuarios` = $id");
    $stmt -> execute();
    return $stmt->fetchAll();
    $stmt->close();
  }

  //CRUDVistaCasetasModel permite ver todas las casetas registradas (Configuracion de datos de Sian Kaan)
  public function CRUDVistaCasetasModel($tabla){
    $stmt = Conexion::conectar()->prepare("SELECT `idcasetas`, `estacion`, `nombre` FROM $tabla ORDER BY $tabla.`estacion` ASC");
    $stmt -> execute();
    return $stmt->fetchAll();
    $stmt -> close();
  }

  //CRUDVistaGuardaparquesModel permite ver todos los guardaparques registrados (Configuracion de datos de Sian Kaan)
  public function CRUDVistaGuardaparquesModel($tabla){
    $stmt = Conexion::conectar()->prepare("SELECT `idguardaparques`, `clave`, `nombre`, `apellido` FROM $tabla ORDER BY $tabla.`nombre` ASC");
    $stmt -> execute();
    return $stmt->fetchAll();
    $stmt -> close();
  }

  public function CRUDVistaGuardaparquesVisitacionModel($id){
    $stmt = Conexion::conectar()->prepare("SELECT `visitacion_guardaparques`.`idvisitacion` AS idvisitacion, `visitacion_guardaparques`.`idguardaparques` AS idguardaparques, `guardaparques`.`nombre` AS nombre FROM `visitacion_guardaparques` LEFT JOIN `guardaparques` ON `guardaparques`.`idguardaparques` = `visitacion_guardaparques`.`idguardaparques` WHERE `visitacion_guardaparques`.`idvisitacion` = $id");
    if ($stmt->execute()) {
      return $stmt->fetchAll();
    }
    $stmt->close();
  }

  //CRUDVistaNacionalidadesModel permite ver todas las nacionalidades en el registro de Visitacion
  public function CRUDVistaNacionalidadesModel($tabla){
    $stmt = Conexion::conectar()->prepare("SELECT `idnacionalidades`, `nacion` FROM $tabla ORDER BY $tabla.`nacion` ASC");
    $stmt -> execute();
    return $stmt->fetchAll();
    $stmt -> close();
  }

  //CRUDActualizarUsuariosModel permite actualizar los datos mostrados.
  public function CRUDActualizarUsuariosModel($datosModel, $tabla, $id){
    $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET `nombre` = :nombre, `apeliido` = :apellido, `correo` = :correo, `contra` = :password WHERE `usuarios`.`idusuarios` = $id");
    $stmt -> bindParam(":nombre", $datosModel['nombre'], PDO::PARAM_STR);
    $stmt -> bindParam(":apellido", $datosModel['apellido'], PDO::PARAM_STR);
    $stmt -> bindParam(":correo", $datosModel['correo'], PDO::PARAM_STR);
    $stmt -> bindParam(":password", $datosModel['password'], PDO::PARAM_STR);
    if ($stmt -> execute()) {
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  //CRUDActualizarCasetasModel permite actualizar una caseta en especifico
  public function CRUDActualizarCasetasModel($datosModel, $tabla){
    $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET `nombre`=:nombre,`estacion`=:estacion WHERE $tabla.`idcasetas` = :id");
    $stmt -> bindParam(":nombre", $datosModel['caseta'], PDO::PARAM_STR);
    $stmt -> bindParam(":estacion", $datosModel['estacion'], PDO::PARAM_STR);
    $stmt -> bindParam(":id", $datosModel['idcaseta'], PDO::PARAM_STR);
    if ($stmt->execute()) {
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  //CRUDActualizarGuardaparquesModel permite actualizar un guardaparques
  public function CRUDActualizarGuardaparqueModel($datosModel, $tabla){
    $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET `nombre`=:nombre,`apellido`=:apellido,`clave`=:clave WHERE $tabla.`idguardaparques` = :id");
    $stmt -> bindParam(":nombre", $datosModel['nombre'], PDO::PARAM_STR );
    $stmt -> bindParam(":apellido", $datosModel['apellido'], PDO::PARAM_STR);
    $stmt -> bindParam(":clave", $datosModel['clave'], PDO::PARAM_STR);
    $stmt -> bindParam(":id", $datosModel['id'], PDO::PARAM_STR);
    if ($stmt->execute()) {
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  //CRUDActualizarVisitacionModel permite actualizar un registro de visitación en base de datos. por ID.
  public function CRUDActualizarVisitacionModel($tipo, $tabla, $datos, $id){
    if ($tipo == 1) {
      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET `fecha`=:fecha,`idcasetas`=:idcasetas WHERE `idvisitacion` = $id");
      $stmt ->bindParam(":fecha", $datos['fecha'], PDO::PARAM_STR);
      $stmt->bindParam(":idcasetas", $datos['caseta'], PDO::PARAM_STR);
      if ($stmt->execute()) {
        return true;
        $stmt->close();
      }else {
        return false;
      }
    }
    if ($tipo == 2) {
      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET `visitantes`=:visitantes,`idnacionalidades`=:nacionalidades WHERE `idvisitacion_visitantes` = $id");
      $stmt->bindParam(":visitantes", $datos['visitantes'], PDO::PARAM_STR);
      $stmt->bindParam(":nacionalidades", $datos['nacionalidad'], PDO::PARAM_STR);
      if ($stmt->execute()) {
        return true;
        $stmt->close();
      }else {
        return false;
      }
    }
  }

  //CRUDBorrarCasetaModel permite eliminar una caseta por el ID.
  public function CRUDBorrarCasetaModel($datosModel, $tabla){
    $stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE $tabla.`idcasetas` = :id");
    $stmt -> bindParam(":id", $datosModel['id'], PDO::PARAM_STR);
    if ($stmt->execute()) {
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  //CRUDBorrarGuardaparqueModel permite eliminar una caseta por ID.
  public function CRUDBorrarGuardaparqueModel($datosModel, $tabla){
    $stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE $tabla.`idguardaparques` = :id");
    $stmt -> bindParam(":id", $datosModel['id'], PDO::PARAM_STR);
    if ($stmt->execute()) {
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }



  //Imprime todas las variables post
  public function allPost(){
    while ($post = each($_POST)) {
      echo "--> post:".$post[0].", valor:".$post[1]." |";
    }
  }

  //Obtiene el ultimo ID de una tabla
  public function ultimoID($nombreid, $tabla){
    $stmt = Conexion::conectar()->prepare("SELECT MAX($nombreid) AS id FROM $tabla");
    if ($stmt->execute()) {
      $result = $stmt->fetch();
      return $result[0];
    }
    $stmt -> close();
  }


  //ajax
  public function ajaxVisitacionAniosModel($tabla,$anio){
    $stmt=Conexion::conectar()->prepare("SELECT MONTH(`fecha`) AS mes, SUM(`visitantes`) AS total FROM $tabla WHERE YEAR(`fecha`) = $anio GROUP BY MONTH(`fecha`) ORDER BY `fecha` ASC");
    $stmt -> execute();
    return $stmt->fetchAll();
    $stmt -> close();
  }

  public function ajaxVisitacionHistoricaModel($tabla){
    $stmt = Conexion::conectar()->prepare("SELECT YEAR(`fecha`) AS anio, SUM(`visitantes`) AS total FROM $tabla GROUP BY anio ASC");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
  }

  public function ajaxVisitacionCasetaAniosModel($tabla, $caseta){
    $stmt = Conexion::conectar()->prepare("SELECT YEAR(`fecha`) AS anio, SUM(`visitantes`) AS visitacion FROM $tabla WHERE `idcaseta` = $caseta GROUP BY caseta ASC, anio ASC");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt ->close();
  }

  public function ajaxVisitacionCasetasHistoricoModel($tabla){
    $stmt = Conexion::conectar()->prepare("SELECT `caseta`, SUM(`visitantes`) AS visitacion FROM $tabla GROUP BY caseta ASC");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
  }

  public function ajaxVehiculosCasetaAnioModel($tabla, $caseta){
    $stmt = Conexion::conectar()->prepare("SELECT YEAR(`fecha`) AS anio, SUM(`bici`) AS bici, SUM(`moto`) AS moto, SUM(`auto`) AS auto, SUM(`carga`) AS carga FROM $tabla WHERE `idcasetas` = $caseta GROUP BY caseta ASC, anio ASC ORDER BY `anio` ASC");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
  }

  public function ajaxVehiculosTotalesModel($tabla){
    $stmt = Conexion::conectar()->prepare("SELECT `caseta`, SUM(`bici`) AS bicis, SUM(`moto`) AS motos, SUM(`auto`) AS autos, SUM(`carga`) AS cargas FROM $tabla GROUP BY caseta ASC ORDER BY `caseta` ASC");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
  }

  public function RVisitacionHistoricaModel($anio, $mes){
    $stmt = Conexion::conectar()->prepare("SELECT SUM(`visitantes`) AS total FROM `vervisitacion` WHERE YEAR(`fecha`) = $anio AND MONTH(`fecha`) = $mes");
    if ($stmt->execute()) {
      return $stmt->fetchAll();
    }
    $stmt->close();
  }

  public function RVisitacionCasetaModel($anio, $caseta){
    $stmt = Conexion::conectar()->prepare("SELECT SUM(`visitantes`) AS visitacion FROM vervisitacion WHERE YEAR(`fecha`) = $anio AND `idcaseta` = $caseta GROUP BY caseta ASC");
    if ($stmt->execute()) {
      $result = $stmt->fetch();
      return $result[0];
    }
    $stmt->close();
  }

  public function RVisitacionNacionalidadesModel(){
    $stmt = Conexion::conectar()->prepare("SELECT `nacionalidad`, SUM(`visitantes`) AS total FROM `vervisitacion` GROUP BY `nacionalidad` ORDER BY `total` DESC LIMIT 10");
    if ($stmt->execute()) {
      return $stmt->fetchAll();
    }
    $stmt->close();
  }

  public function RVisitacionNacionalidadesAnioModel($anio){
    $stmt = Conexion::conectar()->prepare("SELECT `nacionalidad`, SUM(`visitantes`) AS total FROM `vervisitacion` WHERE YEAR(`fecha`) = $anio GROUP BY `nacionalidad` ORDER BY `total` DESC LIMIT 10");
    if ($stmt->execute()) {
      return $stmt->fetchAll();
    }
    $stmt->close();
  }

}
 ?>

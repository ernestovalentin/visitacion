<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Configuración de la Plataforma</title>
</head>
<body>
<style media="screen">
.btn{
  width: 100%;
}
iframe{
  width: 100%;
  height: 80vh;
}
@media only screen and (min-width : 601px) {

}
@media only screen and (min-width : 993px) {

}
@media only screen and (min-width : 1201px) {

}
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="#atras" class="left" onclick="window.history.back()"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Manual de usuario</a>
        <ul class="right">
          <a href="?v=documentacion" class="tooltipped" data-tooltip="Ver documentación del proyecto"><i class="material-icons">book</i></a>
        </ul>
      </div>
    </nav>
  </div>
</header>

<main class="container">
  <div class="card-panel">
    
    <!--<iframe src="https://docs.google.com/document/d/e/2PACX-1vTQhuh8588oTFObqYJeJory_qBGk9eYKVFlFC7nKhM-KBtkmbeZXI3pKhoFPV2-432oZ_yc1Rlpq0uE/pub?embedded=true"></iframe>-->
  </div>
</main>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  console.log("Document ready.");
});

//Onload funciona cuando toda la aplicación ha termiado de cargarse. (Segundo)
window.onload = function() {
  console.log("Windows onload");
};
</script>
</html>

SELECT
  `visitacion_visitantes`.`idvisitacion_visitantes` AS idvisitacion,
  `visitacion`.`idvisitacion` AS idregistro,
  `visitacion`.`fecha` AS fecha,
  `casetas`.`idcasetas` AS idcaseta,
  `casetas`.`nombre` AS caseta,
  `visitacion_visitantes`.`visitantes` AS visitantes,
  `nacionalidades`.`nacion` AS nacionalidad
FROM
  `visitacion`
LEFT JOIN
  `visitacion_visitantes`
    ON
    `visitacion`.`idvisitacion` = `visitacion_visitantes`.`idvisitacion`
LEFT JOIN
	 `nacionalidades`
     ON
     `nacionalidades`.`idnacionalidades` = `visitacion_visitantes`.`idnacionalidades`
LEFT JOIN
	`casetas`
    ON
    `casetas`.`idcasetas` = `visitacion`.`idcasetas`

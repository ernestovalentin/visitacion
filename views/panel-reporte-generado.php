<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Reporte</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
?>
<?php
$reporte = new MainController();
foreach ($reporte->CRUDVistaVisitacionYearMinMaxController() as $key => $value) {
  $minYear = $value['amin'];
  $maxYear = $value['amax'];
}
$preloader = '2';
?>
<body class="grey">
  <style media="screen">
  /* Tamaño carta: 2480 x 3508
  Relacion de aspecto: http://andrew.hedges.name/experiments/aspect_ratio/ */
  .row{
    margin: 0px;
  }
  .A4{
    width: 100vw;
    height: 141vw;
    margin: 0 auto;
    margin-bottom: 20px;
    background-color: white;
    padding: 15px;
  }
  h2{
    font-size: 3vw;
    font-weight: 300;
    margin: 0px;
  }
  h3{
    font-size: 1.5vw;
    margin-bottom: 15px;
  }
  p{
    font-size: 1.2vw;
    margin: 0px;
  }
  td, th{
    padding: 0px;
    font-size: 0.85vw;
  }
  tbody>tr>th{
    font-weight: normal;
  }
  .progress{
    margin-top: 0px;
  }
  @media only screen and (min-width : 601px) {

  }
  @media only screen and (min-width : 993px) {
    .A4{
      width: 80vw;
      height: 113vw;
    }
  }
  @media only screen and (min-width : 1201px) {

  }
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="#atras" class="left" onclick="window.history.back()"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Reporte del año <?php echo $_GET['a'] ?> </a>
      <ul class="right">
        <li><a id="imprimirBoton" class="tooltipped" data-tooltip="Imprimir"><i class="material-icons">&#xE8AD;</i></a></li>
      </ul>
    </div>
  </nav>
</div>
<div class="progress">
  <div class="determinate" style="width:1%"></div>
</div>
</header>
<main class="A4 z-depth-3">
  <div class="row">
    <div class="col s12"><h2 class="center grey-text text-darken-4" style="margin-top:20px;">Comisión Nacional de Áreas Naturales Protegidas.</h2>
      <p class="center grey-text text-darken-1">
        Reporte generado usando la plataforma de Visitación del Complejo Sian Ka'an,
        el día <?php
        setlocale(LC_TIME, "");
        echo utf8_encode(strftime("%A %e de %B del %G")); ?>.
      </p>
    </div>
  </div>
  <div class="row">
    <div class="col s12"><h3>Visitación historica del año  <?php echo $minYear ; ?> al  <?php echo $maxYear; ?> </h3></div>
    <div class="col s12">
      <table class="striped">
        <thead>
          <tr>
            <th></th>
            <th>Enero</th>
            <th>Febrero</th>
            <th>Marzo</th>
            <th>Abril</th>
            <th>Mayo</th>
            <th>Junio</th>
            <th>Julio</th>
            <th>Agosto</th>
            <th>Septiembre</th>
            <th>Octubre</th>
            <th>Noviembre</th>
            <th>Diciembre</th>
            <th>TOTAL</th>
          </tr>
        </thead>
        <tbody>

          <?php
          for ($i = $minYear; $i <= $maxYear; $i++) {
            $total = 0;
            echo "<tr>";
            echo '<td>'.$i.'</td>';
            foreach ($reporte->RVisitacionHistoricaController($i, "1") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "2") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "3") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "4") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "5") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "6") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "7") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "8") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "9") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "10") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "11") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            foreach ($reporte->RVisitacionHistoricaController($i, "12") as $v) {
              if (isset($v['total'])) {
                echo "<td>".$v['total']."</td>";
                $total = $total + $v['total'];
              }else {
                echo "<td>0</td>";
              }
            }
            echo "<td>$total</td>";
            }
            $preloader = "40";
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col s12"><h3>Ingreso por caseta</h3></div>
    <div class="col s6">
      <table class="striped">
        <thead>
          <tr>
            <th></th>
            <?php
            $idcasetas = array();
            foreach ($reporte->CRUDVistaCasetasController() as $key => $v) {
              array_push($idcasetas, $v['idcasetas']);
              echo "<th>".$v['nombre']."</th>";
            }
            ?>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          <?php
          for ($i = $minYear; $i <= $maxYear; $i++) {
            echo "<tr>";
            echo "<td>$i</td>";
            $total3 = 0;
            for ($x=1; $x <= count($idcasetas) ; $x++) {
              $visitacion = $reporte->RVisitacionCasetaController($i, $x);
              if (!empty($visitacion)) {
                echo "<td>$visitacion</td>";
                $total3 = $total3 + $visitacion;
              }else {
                echo "<td>0</td>";
              }
            }
            echo "<td>".$total3."</td>";
            echo "</tr>";
          }
          $preloader = "70";
           ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col s6">
      <h3>Principales nacionalidades del año <?php echo $_GET['a'] ?> </h3>
      <table class="striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Nacionalidad</th>
            <th>Visitantes</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($reporte->RVisitacionNacionalidadesAnioController($_GET['a']) as $key => $v) {
            echo "<tr>";
            echo "<td>".++$key."</td>";
            echo "<td>".$v['nacionalidad']."</td>";
            echo "<td>".$v['total']."</td>";
            echo "</tr>";
          }
           ?>
        </tbody>
      </table>
    </div>
    <div class="col s6">
      <h3>Principales nacionalidades</h3>
      <table class="striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Nacionalidad</th>
            <th>Visitantes</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($reporte->RVisitacionNacionalidadesController() as $key => $v) {
            echo "<tr>";
            echo "<td>".++$key."</td>";
            echo "<td>".$v['nacionalidad']."</td>";
            echo "<td>".$v['total']."</td>";
            echo "</tr>";
          }
           ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<form class="hide" action="?panel=pdf" method="post">
  <input type="text" id="html" name="html">
</form>
</body>
<script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  function preloader(){
    $('.determinate').width(<?php echo '"'.$preloader.'%"'?>);
    console.log();
  }
  preloader();
  //setInterval(preloader, 1000);

$('#imprimirBoton').click(function(event) {
  var html = $('.A4').html();
  $('#html').val(html);
  $('form').submit();
});


});
</script>
<?php

 ?>
</html>

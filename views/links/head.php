<!--Importar Google Icons Font
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->
<link rel="stylesheet" href="node_modules/material-design-icons/iconfont/material-icons.css">
<!--Importar estilos de Materialize Framework--->
<link type="text/css" rel="stylesheet" href="views/css/materialize.css">
<!--Enseña al navegador que el sitio web está optimizado para mobiles-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--Enseña al navegador que el contenido es html codificado en UTF-8 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--Enseña al navegador que el sitio web es Compatible con IE-->
<meta http-equiv="X-UA-Compatible" content="ie=edge">

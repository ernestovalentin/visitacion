<?php
if (isset($_POST['imagen'])) {
  $datos = base64_decode(
    preg_replace('/^[^,]*,/', '', $_POST['imagen'])
  );
  file_put_contents('views/img/imagen-test.png', $datos);
  die('OK');
}
//https://es.stackoverflow.com/questions/99146/guardar-imagen-canvas-autom%C3%A1ticamente-en-servidor-con-php
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <style>
  body {
    background: white;
    padding: 16px;
  }
  canvas {
    border: 1px dotted black;
  }
  .chart-container {
    position: relative;
    margin: auto;
    height: 80vh;
    width: 80vw;
  }
  </style>
  <button onclick="guardar()" id="convertir">Convertir</button>
  <div class="chart-container">
    <canvas id="chart" width="900" height="300"></canvas>
  </div>
  <a id="imagen" href="views/img/imagen-test.png" target="_blank">Ver imagen generada</a>
  <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="node_modules/chart.js/dist/Chart.min.js"></script>
  <script type="text/javascript" src="views/js/dataCharts.js"></script>
  <script>
  var canvas = document.getElementById("chart");
  var ctx = canvas.getContext("2d");
  var labelsT = [], dataT=[];
  labelsT.push(Mount[1]);
  labelsT.push(Mount[2]);
  labelsT.push(Mount[3]);
  labelsT.push(Mount[4]);
  labelsT.push(Mount[5]);
  dataT.push(parseInt("1"));
  dataT.push(parseInt("2"));
  dataT.push(parseInt("3"));
  dataT.push(parseInt("4"));
  dataT.push(parseInt("5"));
  var grafica = new CrearGrafica();
  grafica.crearData(labelsT, "grafica1", dataT);
  grafica.crearOpciones();
  grafica.crear("bar");

  $( '#imagen' ).hide();

  function guardar() {
    $('#chart-container').width('900px').height('300px');
    var imagen = canvas.toDataURL("image/png");
    /* Envío la petición XHR al servidor con los datos de la imagen */
    $.ajax({
      url: "<?= $_SERVER['PHP_SELF'] ?>",
      method: 'post',
      data: { imagen: imagen},
    }).done(function(retorno) {
      alert(retorno);
      $( '#imagen' ).show();
    });
  }

</script>
</body>
</html>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <?php include "links/head.php" ?>
  <title>Panel de Visitación</title>
</head>
<?php session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
} ?>
<body class="light-green lighten-5">
  <nav>
    <div class="nav-wrapper container">
      <a class="brand-logo center hide-on-small-only">
        <img src="views/img/tec-blanco.png" alt="Logo del Tecnológico" height="64px" class="hide-on-med-and-down">
        <img src="views/img/conanp-blanco.png" alt="Logo de la CONANP" height="64px">
        <img src="views/img/semarnat-blanco.png" alt="Logo de SEMARNAT" height="64px" class="hide-on-med-and-down">
      </a>
      <ul class="right">
        <a href="?v=salir"><i class="material-icons">exit_to_app</i></a>
      </ul>
    </div>
  </nav>
  <main>
    <div class="row container">
      <div class="col s12">
        <h1 style="font-weight:200">Bienvenido <?php echo $_SESSION['nombre']; ?></h1>
        <p class="flow-text center">Administre información del acceso y actividades del Complejo Sian Ka’an.</p>
      </div>
    </div>
    <div class="row section container center">
      <div class="col s12 m6 l3">
        <div class="card-panel hoverable" onclick="location.href='?panel=registrar-visitacion'" style="cursor:pointer;">
          <i class="large material-icons">transfer_within_a_station</i>
          <p class="flow-text">Registrar visitación</p>
          <p>Registra información del flujo de visitantes al Complejo Sian Ka’an</p>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="card-panel hoverable" onclick="location.href='?panel=grafica'" style="cursor:pointer;">
          <i class="large material-icons">insert_chart</i>
          <p class="flow-text">Generar Gráficas</p>
          <p>Usa información del registro de flujo de visitantes para crear diferentes tipos de gráficas, en las fechas que desee.</p>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="card-panel hoverable" onclick="location.href='?panel=reporte'" style="cursor:pointer;">
          <i class="large material-icons">description</i>
          <p class="flow-text">Generar reportes</p>
          <p>Utiliza las gráficas creadas para realizar informes, agregando información adicional para crear un buen reporte especifico.</p>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="card-panel hoverable" onclick="location.href='?panel=configuracion'" style="cursor:pointer;">
          <i class="large material-icons">settings</i>
          <p class="flow-text">Configuración</p>
          <p>Entre a la configuración de la plataforma, donde puede cambiar su correo, elegir una nueva contraseña. Y agregar o eliminar casetas y guardaparques.</p>
        </div>
      </div>
    </div>
  </main>
</body>
<?php include "links/foot.php" ?>
</html>

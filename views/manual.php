<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Manual de la Plataforma de Visitación</title>
</head>
<body>
  <style media="screen">
  h1{
    font-size: 3rem;
    font-weight: bold;
    margin-top: 10rem;
  }
  h2{
    font-size: 2.3rem;
    font-weight: bold;
    margin-left: 1rem;
    margin-top: 5rem;
  }
  h3{
    font-size: 2rem;
    font-weight: 300;
    margin-left: 2rem;
  }
  code{
    padding: 0.3rem 0.8rem 0.3rem 0.8rem;
    color: #303030;
    background-color: #EEEEEE;
    font-size: 1rem;
    font-weight: 400;
    font-family: monospace;
    border-radius: 5px;
  }
  img{
    max-width: 100%;
    height: auto;
  }
  @media only screen and (min-width : 601px) {

  }
  @media only screen and (min-width : 993px) {
    #index{
      position: fixed;
      right: 0;
    }
  }
  @media only screen and (min-width : 1201px) {

  }
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="#atras" class="left" onclick="window.history.back()"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Servicio de ayuda</a>
        <ul class="right">
          <a href="?v=documentacion" class="tooltipped" data-tooltip="Ver documentación del proyecto"><i class="material-icons">book</i></a>
        </ul>
      </div>
    </nav>
  </div>
</header>
<div class="row">
  <div class="col s12 l3 card-panel" id="index">
    <ul class="section table-of-contents">
      <li>Índice</li>
      <li><a href="#introduccion">Introduccion</a></li>
      <li><a href="#instalacion">A) Instalación</a></li>
      <li><a href="#primeros-pasos">B) Primeros pasos</a></li>
      <li><a href="#registrar-caseta">C) Registrar casetas</a></li>
      <li><a href="#registrar-guardaparque">D) Registrar guardaparques</a></li>
      <li><a href="#registrar-visitacion">E) Registrar visitacion</a></li>
      <li><a href="#generar-graficas">F) Generar gráficas</a></li>
      <li><a href="#generar-reporte">G) Generar reporte</a></li>
      <li><a href="#agradecimientos">Agradecimientos</a></li>
    </ul>
    <a href="https://ernestovalent.gitbooks.io/manual-visitacion/content/">Ver en web</a>
  </div>
  <div class="col s12 l9 card-panel">








<!-- Introduccion-->
<div class="scrollspy" id="introduccion">
  <h1 class="scrollspy" style="margin-top:10px;">Introducción</h1>
  <p>Manual para el usuario de la Plataforma <em>&quot;Visitación&quot;</em> perteneciente a la Comisión Nacional de Áreas Naturales Protegidas de Felipe Carrillo Puerto, como parte del Proyecto de Residencia Profesional <em>&quot;Plataforma Web para Administrar la Información de Acceso y Actividades del Complejo Sian Ka&#39;an&quot;</em> del Instituto Tecnológico Superior de Felipe Carrillo Puerto. Desarrollado por Ernesto Valentin Caamal Peech.</p>
  <blockquote>
  <p>Este manual es un servicio de ayuda a los usuarios que lo utilizan, adicional pueden consultar la sección de Preguntas Frecuentes.</p>
  </blockquote>
  <p>Este manual además se encuentra contenido en el proyecto, en la sección: Para usuarios registrados <code>Panel -&gt; Configuración -&gt; Manual de la Plataforma.</code> Para los usuarios no registrados desde <code>Inicio -&gt; Documentación.</code></p>
  <hr>
  <p>Para atención técnica especializada, solicitar corrección de errores o solicitar agregar nuevas funcionalidades al sistema, puede consultar al desarrollador de proyecto.</p>
  <p>Datos de contacto con el Desarrollador: Ernesto Valentin Caamal Peech</p>
  <p>Correo: ernesto.valen.t@gmail.com</p>
</div>












    <!--Instalacion.md -->
    <div class="scrollspy" id="instalacion">
    <h1>A) Instalación</h1>
    <p>Se puede realizar una instalación limpia de la Plataforma Visitación utilizando las siguientes tecnologías:</p>
    <ol>
    <li><a href="#descargar">Github (Opcional)</a></li>
    <li><a href="#entorno-virtual">XAMPP (O cualquier otro entorno virtual de Apache)</a></li>
    <li><a href="#phpmyadmin">phpMyAdmin</a></li>
    <li><a href="#node">Node (NPM)</a></li>
    </ol>
    <h2 id="1-descargar-descargar-">Descargar</h2>
    <p>Primero obtenemos el código fuente del proyecto de la Plataforma Visitación. Existen dos formas de obtenerlo, utilizando el software de código abierto Github Desktop (recomendado para descargar futuras actualizaciones), o simplemente descargando la última versión disponible en <a href="https://github.com/ernestovalent/visitacion/releases/latest">https://github.com/ernestovalent/visitacion/releases/latest</a> (descarga directa).</p>
    <h3 id="github-desktop">Github Desktop</h3>
    <p>Este software permite descargar el código fuente de la Plataforma y administrar nuevas descargas en futuras versiones de la Plataforma. Hay que tener en cuenta que con este método de instalación se pueden obtener las versiones más recientes incluso versiones de pruebas que pueden resultar en errores.</p>
    <p>Descargar Github Desktop desde la web oficial: <a href="https://desktop.github.com/">https://desktop.github.com/</a> Disponible únicamente para sistemas operativos MacOs y Windows.<img src="https://desktop.github.com/images/github-desktop-screenshot-windows.png" alt="">La instalación de este software no es diferente a cualquier otro Software.</p>
    <p>Para configurar Github Desktop deberá iniciar sesión con una cuenta de <a href="https://github.com/join?source=header-home">Github.com</a>, después de iniciar puede clonar un proyecto desde una URL:<code>ernestovalent/visitacion</code></p>
    <p><img src="assets/clone.PNG" alt=""></p>
    <p>También puede descargar el proyecto desde la linea de comandos de Git usando</p>
    <p><code>git clone</code><a href="https://github.com/ernestovalent/visitacion"><code>https://github.com/ernestovalent/visitacion</code></a><code> </code></p>
    <p>Asegúrese que la ruta de instalación corresponda a la ruta del servidor virtual Apache disponible. Si aún no tiene instalado el servidor se recomienda instalar XAMPP.</p>
    <blockquote>
    <p>Este paso solo es la descarga. Para ejecutar la Plataforma se requiere la instalación del servidor local, como se muestra en el <a href="#entorno-virtual">siguiente tema</a>. Posteriormente se requiere de la instalación de dependencias con NPM.</p>
    </blockquote>
    <h3 id="-ltima-versi-n-disponible">Última versión disponible</h3>
    <p>Se puede descargar la última versión disponible de la Plataforma en el enlace: <a href="https://github.com/ernestovalent/visitacion/releases/latest">https://github.com/ernestovalent/visitacion/releases/latest</a></p>
    <p>Se descarga la versión con extensión .zip</p>
    <p><a href="https://github.com/ernestovalent/visitacion/releases/latest"><img src="assets/source.PNG" alt=""></a></p>
    <p>Se descomprime en la ruta de del servidor local.</p>
    <blockquote>
    <p>Este paso solo es la descarga. Para ejecutar la Plataforma se requiere la instalación del servidor local, como se muestra en el <a href="#entorno-virtual">siguiente tema</a>. Posteriormente se requiere de la instalación de dependencias con NPM.</p>
    </blockquote>
    <h2 id="2-instalar-entorno-virtual-entorno-virtual-">Instalar entorno virtual</h2>
    <p>Un entorno de servidor virtual es capaz de ejecutar y compilar una Plataforma Web. Usted puede descargar e instalar un entorno de servidor virtual multiplataforma mediante el Software XAMPP disponible en: <a href="https://www.apachefriends.org/es/download.html">https://www.apachefriends.org/es/download.html </a> <a href="https://www.apachefriends.org/es/download.html"><img src="https://d16zszyyqlzz6z.cloudfront.net/images/stamps/stamp-173x38-f087cb4d.gif" alt=""></a></p>
    <p>Para configurar XAMPP después de la instalación se necesita iniciar la aplicación e iniciar los servicios <strong>MySQL Server</strong> y <strong>Apache Web Server</strong>.</p>
    <p><img src="assets/server.png" alt=""></p>
    <p>También será necesario copiar la carpeta del proyecto en el directorio htdocs de XAMPP, el cual es el directorio raíz del servidor web. Por lo que todo los proyectos que contenga el directorio serán públicos en una red local. Si usted ha decidido descargar el proyecto mediante Github Desktop deberá seleccionar la ruta del directorio htdocs antes de descargar. Si por otra parte elige descargar la última versión mediante el sitio donde se aloja el proyecto, usted deberá descomprimir los archivos en el directorio manualmente.</p>
    <table>
    <thead>
    <tr>
    <th style="text-align:left">Directorio en Github Desktop</th>
    <th style="text-align:left">Descomprimir manualmente al directorio</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td style="text-align:left"><img src="assets/clone.PNG" alt=""></td>
    <td style="text-align:left"><img src="assets/extraer.PNG" alt=""></td>
    </tr>
    </tbody>
    </table>
    <p>Al finalizar la instalación será posible visualizar el proyecto en cualquier navegador web utilizando la siguiente URL: <code>http://localhost/visitacion/</code></p>
    <h2 id="3-phpmyadmin-phpmyadmin-">phpMyAdmin</h2>
    <p>phpMyAdmin es un componente de XAMPP que se ejecuta al iniciar el servicio MySQL Database, que permite administrar una base de datos relacional de MySQL. Para entrar al componente deberá entrar a cualquier navegador utilizando la siguiente URL: <code>http://localhost/phpmyadmin/</code></p>
    <p>La Plataforma Visitación utiliza Base de Datos MySQL para poder registrar, leer, modificar y eliminar información entre sus diferentes módulos. Por lo que es necesario crear una base de datos con las tablas específicas del proyecto. Para simplificar este proceso ha sido creado un script de sql que será importado a la base de datos.</p>
    <ol>
    <li>Importar el script a phpMyAdmin, en sección de importar <img src="assets/importar.png" alt=""> y seleccione el archivo a importar <img src="assets/archivoimportar.png" alt=""></li>
    <li>Seleccione el archivo script de base de datos<code>/visitacion/db/db.sql</code><br> <img src="assets/db.png" alt=""></li>
    <li>Continuar para ejecutar el Script: <img src="assets/continuar.png" alt=""></li>
    <li>Al ejecutar el Script correctamente, muestra el siguiente mensaje: <img src="assets/exitosamente.png" alt=""></li>
    <li><p>Para ingresar por primera vez a la Plataforma, es necesario crear un nuevo usuario: Entrar a la Base de Datos &quot;Visitación&quot; y entrar a la tabla de &quot;Usuarios&quot;. Ahí podrá registrar un nuevo usuario. <img src="assets/insertar.PNG" alt=""><br><img src="assets/tablas.PNG" alt=""></p>
    </li>
    <li><p>Se necesitan registrar al menos 4 usuarios; 1 de Soporte Técnico, 1 como Administrador (Programa de Uso Público) y 2 que corresponde a Dirección y Subdirección.<br><img src="assets/registroadmin.PNG" alt=""></p>
    </li>
    <li><p>Ahora ya ha creado la base de datos, puede salir con seguridad de la ventana. Al finalizar la instalación y entrar a la Plataforma, podrá iniciar con el correo y contraseña especificados. </p>
    </li>
    </ol>
    <h2 id="4-node-js-node-">Node JS</h2>
    <p>Node JS es un entorno de ejecución de Javascript y que es utilizado en el proyecto como gestor de paquetes gracias a las funciones con NPM. Este es el último paso de la instalación, debido a que la Plataforma hace referencias a librerías externas de Javascript importantes para funcionar.</p>
    <p>Para instalar Node Js, descargar en <a href="https://nodejs.org/es/download/">https://nodejs.org/es/download/</a> e instalar. Al finalizar la instalación puede abrir una ventana del terminal (CMD) y verificar que se puedan ejecutar los comandos siguientes:</p>
    <pre><code class="lang-bash">
    node --version
    npm --version
    </code></pre>
    <p>La linea de comandos deberá mostrar la versión instalada en el sistema:</p>
    <p><img src="assets/nodeversion.PNG" alt=""></p>
    <p>Terminado la comprobación ahora podemos navegar hasta la ruta donde se encuentra el proyecto en la dirección del servidor local (XAMPP): <code>cd \xampp\htdocs\visitacion</code></p>
    <p>y ejecutar</p>
    <code>npm install</code>
    <p>Al finalizar la instalación se crea una carpeta llamada node_modules quedando de esta forma:</p>
    <p><img src="assets/node_modules.PNG" alt=""></p>
    <p>Y ahora se puede iniciar la Plataforma en <code>http://localhost/visitacion/</code></p>
    <p>Si al iniciar la Plataforma muestra el siguiente mensaje, </p>
    <code>Error en la instalación, faltan agregar dependencias de node: Ejecutar npm install en linea de comandos.</code>
    <p>Por favor verifica que la instalación mediante Node JS ha finalizado correctamente. Si es necesario vuelva a ejecutar el comando en la ruta especifica del proyecto: <code>npm install</code></p>
</div>















  <!-- Primeros pasos -->
  <div class="scrollspy" id="primeros-pasos">
  <h1 id="primeros-pasos">B) Primeros pasos.</h1>
  <p>Si usted ha seguido correctamente la instalación habrá registrado a 4 nuevos usuarios en la base de datos, los cuales tienen los permisos suficientes para iniciar sesión y realizar uso de las funciones de la Plataforma.</p>
  <p>La lista de funciones de la Plataforma Visitación es la siguiente:</p>
  <h2 id="funciones-de-visitaci-n-v0-5">Funciones de Visitación v0.5</h2>
  <ul>
  <li>Registrar flujo de visitantes a la Reserva.<ul>
  <li>Registra la fecha, la caseta, los guardaparques y visitación.</li>
  </ul>
  </li>
  <li>Registrar vehículos entrantes a la Reserva.<ul>
  <li>Registra automóviles, bicicletas, motos y camiones de carga o vehículos pesados.</li>
  </ul>
  </li>
  <li>Generar gráficas.</li>
  <li>Generar reportes.</li>
  <li>Permite realizar cambios a la configuración.<ul>
  <li>Permite modificar los datos de los 3 usuarios principales.</li>
  <li>Permite administrar datos de Sian Ka&#39;an, como las Casetas y los Guardaparques.</li>
  <li>Permite consultar la documentación.</li>
  </ul>
  </li>
  </ul>
  <h2 id="iniciar-por-primera-vez">Iniciar por primera vez</h2>
  <p>Cuando inicias por primera vez en la plataforma se muestra una ventana similar a esta:<img src="assets/inicio.png" alt=""></p>
  <p>Por la cual, podrá INICIAR la Plataforma donde primeramente podrá Iniciar Sesión usando uno de los cuatro usuarios registrados. <img src="assets/iniciar.png" alt=""></p>
  <p>Al oprimir el botón de INICIAR, y si el usuario y contraseña es el correcto, podrá visualizar el Panel de la Plataforma Visitación. El cual presentamos a continuación.<img src="assets/panel.png" alt=""> <br> El Panel de Visitación ofrece los 4 botones principales con las funciones que le corresponden, en cada una de ellas se despliegan funciones principales y secundarias sin embargo es muy necesario seguir un orden entre las funciones.</p>
  <h3>El orden es el siguiente:</h3>
  <ol>
  <li><p>Registrar las casetas de Sian Ka&#39;an.</p>
  </li>
  <li><p>Registrar los guardaparques de Sian Ka&#39;an.</p>
  </li>
  <li><p>Registrar visitación (incluye vehiculos).</p>
  </li>
  <li><p>Generar gráficas.</p>
  </li>
  <li><p>Generar reportes.</p>
  </li>
  </ol>
  <p>Es muy importante y necesario seguir este orden.</p>
  <p>Para aprender como realizar estas funciones obligatorias, revisar el siguiente sección.</p>
</div>

















<!-- Registrar caseta-->
<div class="scrollspy" id="registrar-caseta">
<h1>C) Registrar una nueva caseta</h1>
<p>Para agregar una caseta necesariamente deberá iniciar sesión, al entrar al Panel diríjase al botón de configuración y posteriormente a la opción de &quot;Administrar datos de Sian Ka&#39;an&quot;.</p>
<p>O simplemente escriba esta dirección en su navegador: <code>http://localhost/visitacion/?configurar=siankaan</code></p>
<p>Los pasos son los siguientes:</p>
<ol>
<li>Iniciar sesión en la Plataforma.</li>
<li><p>En el Panel de la Plataforma, oprimir el botón de configuración.<br><img src="assets/configuracion.png" alt=""></p>
</li>
<li><p>Entre las opciones de configuración, busque &quot;Administrar datos de Sian Ka&#39;an&quot; y oprima su respectivo botón.<img src="assets/configuracion-siankaan.png" alt=""></p>
</li>
<li><p>Si aún no hay datos registrados le saldrá el siguiente mensaje.<br><img src="assets/caseta-sinregistro.png" alt=""></p>
</li>
<li><p>Para agregar una nueva caseta, diríjase al botón localizado en la parte inferior a la derecha de color amarillo.<br><img src="assets/boton-registrar.png" alt=""></p>
</li>
<li><p>Seleccione la opción con el ícono de &quot;Agregar caseta&quot;.<br><img src="assets/agregar-caseta.png" alt=""></p>
</li>
<li><p>Aparecerá un formulario que deberá llenar con los datos de la nueva caseta a registrar. *El nombre es obligatorio.<br><img src="assets/formulario-agregar-caseta.png" alt=""></p>
</li>
<li><p>Oprimir el botón de guardar.</p>
</li>
<li><p>Al cargar nuevamente aparecerá la caseta registrada.<br><img src="assets/registro-caseta.png" alt=""></p>
</li>
</ol>
<h2 id="editar-una-caseta-registrada">Editar una caseta registrada</h2>
<p>En esta misma sección, después de agregar una caseta, puede editar la información registrada dando clic en el botón con el icono de editar. <img src="assets/editar.png" alt=""><img src="assets/editar.png" alt=""></p>
<p>Al oprimir el botón inmediatamente se habilita la opción de editar la información.<img src="assets/editar-caseta.png" alt=""></p>
<p>Después de realizar las modificaciones, puede oprimir el botón con el icono de guardar.</p>
<h2 id="eliminar-una-caseta-registrada">Eliminar una caseta registrada</h2>
<p>En esta misma sección puede eliminar una caseta.</p>
<p>Esta función es muy delicada por las consecuencias que puede causar a los reportes. Se sugiere no eliminar una caseta a la que se le han asignado visitantes. Esto debido que al eliminar una caseta no saldrá en los reportes. Se sugiere en su caso, modificar una caseta existente, o crear una nueva caseta y dejar de utilizar la anterior.</p>
<p>Para eliminar una caseta existente, basta con oprimir el botón con el icono de eliminar,  <img src="assets/eliminar.png" alt=""></p>
<p>Aparece el siguiente mensaje de confirmación del cual oprimimos el botón &quot;Borrar&quot;:</p>
<p><img src="assets/mensaje-eliminar-caseta.png" alt=""></p>
</div>
























<!-- Registrar guardaparque-->
<div class="scrollspy" id="registrar-guardaparque">
<h1>D) Registrar un nuevo guardaparque</h1>
<p>Para agregar un nuevo guardaparque necesariamente deberá iniciar sesión, al entrar al Panel diríjase al botón de configuración y posteriormente a la opción de &quot;Administrar datos de Sian Ka&#39;an&quot;.</p>
<p>O simplemente escriba esta dirección en su navegador: <code>http://localhost/visitacion/?configurar=siankaan</code></p>
<p>Los pasos son los siguientes:</p>
<ol>
<li>Iniciar sesión en la Plataforma.</li>
<li><p>En el Panel de la Plataforma, oprimir el botón de configuración.<br><img src="assets/configuracion.png" alt=""></p>
</li>
<li><p>Entre las opciones de configuración, busque &quot;Administrar datos de Sian Ka&#39;an&quot; y oprima su respectivo botón.<img src="assets/configuracion-siankaan.png" alt=""></p>
</li>
<li><p>Si aún no hay datos registrados le saldrá el siguiente mensaje.<br><img src="assets/guardaparque-sinregistro.png" alt=""></p>
</li>
<li><p>Para agregar un nuevo guardaparque, diríjase al botón localizado en la parte inferior a la derecha de color amarillo.<br><img src="assets/boton-registrar.png" alt=""></p>
</li>
<li><p>Seleccione la opción con el ícono de &quot;Agregar guardaparque&quot;.<br><img src="assets/agregar-guardaparque.png" alt=""></p>
</li>
<li><p>Aparecerá un formulario que deberá llenar con los datos del guardaparque a registrar. *El nombre y apellido es obligatorio.<br><img src="assets/formulario-agregar-guardaparque.png" alt=""></p>
</li>
<li><p>Oprimir el botón de guardar.</p>
</li>
<li><p>Al cargar nuevamente aparecerá el guardaparque registrada.</p>
</li>
</ol>
</div>























<!-- Registrar visitacion-->
<div class="scrollspy" id="registrar-visitacion">
<h1 id="registrar-visitaci-n">E) Registrar visitación</h1>
<p>Visitación es el nombre con que se le conoce al acceso de los visitantes nacionales e internacionales a la Reserva Sian Ka&#39;an.</p>
<p>El proceso de registro de visitantes es realizado con la colaboración de los guardaparques, quienes en cada caseta de acceso deberán anotar el número de visitantes y la nacionalidad proveniente. Este proceso es realizado manualmente en hojas de papel, en donde posteriormente serán registrados en la Plataforma Visitación.</p>
<p>Para registrar el flujo de visitantes en la Plataforma Visitación se siguen los pasos siguientes:</p>
<ol>
<li>Iniciar Sesión en la Plataforma</li>
<li><p>En el panel, oprimir el botón de Registrar visitación.<br><img src="assets/registrar-visitacion.png" alt=""></p>
</li>
<li><p>El siguiente es un formulario para registrar el flujo de visitantes. Para realizar el proceso se divide en tres secciones, la primera de información general y obligatoria, la segunda sección del flujo de visitantes y la tercera de información opcional.</p>
<ol>
<li><p>En la información general se solicita los datos generales al registro como la fecha, la caseta de acceso y los guardaparques que registran. *Todos los datos son obligatorios.<br><img src="assets/visitacion-1.png" alt=""></p>
</li>
<li><p>En información del flujo de visitantes se solicitan los datos de los visitantes como el número de visitantes y su nacionalidad, al oprimir en el campo de &quot;número&quot; se genera otra fila para seguir agregando registros. *Todos los campos son obligatorios, por lo que deberá eliminar una fila si no contiene información.<br><img src="assets/visitacion-2.png" alt=""></p>
</li>
<li><p>En información opcional se solicitan datos referentes al registro de visitación del día. En esta sección se solicitan datos como la cantidad de vehículos entre autos, bicicletas, motos y vehículos de carga. Así como agregar un comentario. *Los datos en este formulario no son obligatorios, por lo que pueden o no estar vacíos.<br><img src="assets/visitacion-3.png" alt=""></p>
</li>
</ol>
</li>
<li><p>Para guardar el registro puede oprimir en el botón de &quot;SIGUIENTE&quot;, la Plataforma Visitación guardará si los campos obligatorios están escritos, de lo contrario no realizará ninguna acción.</p>
</li>
</ol>
<h2 id="editar-visitaci-n">Editar visitación</h2>
<p>La Plataforma Visitación ofrece la posibilidad de visualizar los registros guardados, y en versiones futuras la posibilidad de editar el registro.</p>
<p>Para acceder al panel de edición deberá buscar el botón con el icono de buscar en la parte superior a la derecha.<br><img src="assets/boton-editar-registros.png" alt=""></p>
<p>El panel de edición de registros de visitantes es el siguiente:<br><img src="assets/vista-editar-id.png" alt=""></p>
<p>Para encontrar un registro específico de entre varios registros deberá buscarlo en un rango de fechas, seleccionando la fecha inicial y la fecha final. De la siguiente forma:<img src="assets/visitacion-id-fecha.png" alt=""></p>
<p>Después de elegir el rango de fechas, puede oprimir el botón de &quot;VER&quot; para mostrar los resultados de búsqueda:<img src="assets/visitacion-id-resultados.png" alt=""></p>
<p>Para visualizar y editar un registro específico, basta con oprimir el botón de editar de lado derecho, el cual llevará a la vista de editar.<img src="assets/visitacion-editar-id.png" alt=""></p>
<p>Ahora puede editar y guardar el registro oprimiendo en el botón de &quot;GUARDAR&quot;.</p>
<p>*Esta funcionalidad estará disponible en siguientes versiones del producto.</p>
</div>






















<!-- Generar gràfica -->
<div class="scrollspy" id="generar-graficas">
<h1 id="generar-gr-ficas">F) Generar gráficas</h1>
<p>Puede utilizar la Plataforma Visitación para generar gráficas utilizando información registrada del flujo de visitantes.</p>
<h2 id="tipos-de-gr-ficas">Tipos de Gráficas</h2>
<p>Las gráficas que puede generar en la Plataforma Visitación V0.5 son:</p>
<ul>
<li>Visitación por cada año.<ul>
<li>Genera una gráfica mostrando la visitación por meses en el año especificado.<br><img src="assets/grafica-1.png" alt=""></li>
</ul>
</li>
<li>Visitación histórica.<ul>
<li>Genera una gráfica mostrando el total de visitantes en cada año registrado.<br><img src="assets/grafica-2.png" alt=""></li>
</ul>
</li>
<li>Visitación de caseta histórica.<ul>
<li>Genera una gráfica mostrando el total de visitantes de cada año registrado de una caseta en especifico.<br><img src="assets/grafica-3.png" alt=""></li>
</ul>
</li>
<li>Visitación total entre casetas.<ul>
<li>Genera una gráfica mostrando el total de visitantes de cada caseta por todos los años registrados.<br><img src="assets/grafica-4.png" alt=""></li>
</ul>
</li>
<li>Entrada de vehículos en una caseta.<ul>
<li>Genera una gráfica mostrando el total de todos los vehículos en cada año registrado.<br><img src="assets/grafica-5.png" alt=""></li>
</ul>
</li>
<li>Tipos de vehículos histórico.<ul>
<li>Genera una gráfica mostrando el total de vehículos ordenados en cada caseta de acceso.<br><img src="assets/grafica-6.png" alt=""></li>
</ul>
</li>
</ul>
<h2 id="pasos-para-generar-una-gr-fica">Pasos para generar una gráfica</h2>
<p>Para generar una gráfica se utilizan los pasos siguientes.</p>
<ol>
<li>Iniciar Sesión en la Plataforma Visitación.</li>
<li><p>En el Panel, oprimir el botón de generar gráfica.<br><img src="assets/generar-grafica.png" alt=""></p>
</li>
<li><p>Al entrar en la vista de generar gráfica se muestra lo siguiente:<br><img src="assets/vista-generar-grafica.png" alt=""></p>
</li>
<li><p>A continuación elige un tipo de dato de los tipos disponibles.</p>
</li>
<li><p>Elige el tipo de gráfica entre gráfica de barras o gráfica lineal. (Disponibles en esta versión).</p>
</li>
<li><p>*Opcional, puede seleccionar un año en específico si se le solicita.<br><img src="assets/elige-fecha.png" alt=""></p>
</li>
<li><p>*Opcional, puede seleccionar una caseta en específico si se le solicita.<br><img src="assets/elige-caseta.png" alt=""></p>
</li>
<li><p>Oprimir el botón de &quot;GENERAR&quot; para generar la gráfica configurada. Si ha olvidado seleccionar algo, el validador muestra un mensaje.</p>
</li>
<li><p>El navegador tomará unos segundos para mostrar los resultados. Las gráficas utilizan información registrada en la Base de Datos. Si existe algún problema al mostrar o configurar las gráficas es posible que se deba a la configuración de su navegador al tener desactivado el uso de Javascript.</p>
</li>
</ol>
<h2 id="guardar-una-gr-fica-generada-">Guardar una gráfica generada.</h2>
<p>En la versión actual de la Plataforma Visitación solo es posible guardar a través del navegador. Para lograr guardar una imagen deberá hacer clic derecho con el mouse en la gráfica y seleccionar la opción de &quot;Guardar imagen como..&quot;. Esta función funciona en la mayoría de los navegadores actuales excepto en Internet Explorer de Microsoft.<br><img src="assets/guardar-grafica.png" alt=""></p>
<p>*En las siguientes versiones se espera automatizar el proceso de guardado en el servidor para incluirlo en los reportes.</p>
</div>
























<!-- Generar reportes -->
<div class="scrollspy" id="generar-reporte">
<h1>G) Generar reporte</h1>
<p>La Plataforma Visitación utiliza toda la información guardada en la Base de Datos para generar un reporte de informes con los datos solicitados por el Departamento de Uso Público de la CONANP. Al final se obtiene un archivo de formato documento portable (PDF).</p>
<p>El reporte incluye:</p>
<ul>
<li>Visitación histórica de todos los años disponibles.</li>
<li>Ingreso por caseta de todos los años disponibles.</li>
<li>Principales nacionalidades del año seleccionado.</li>
<li>Principales nacionalidades de todos los años disponibles.</li>
</ul>
<h2 id="pasos-para-generar-un-reporte">Pasos para generar un reporte</h2>
<ol>
<li>Iniciar Sesión en la Plataforma Visitación.</li>
<li><p>En el Panel, oprimir el botón de generar reporte. (Será necesario haber realizado registros de visitación).<br><img src="assets/generar-reporte.png" alt=""></p>
</li>
<li><p>Al entrar a la vista de generar reporte se muestra el siguiente asistente de configuración.<br><img src="assets/vista-generar-reporte.png" alt=""></p>
</li>
<li><p>Puede seleccionar el año para generar un reporte. Usted puede generar un reporte en cualquier fecha del año, ya sea semestral, cuatrimestral o incluso semanal, sin embargo la Plataforma Visitación tomará información de los registros realizados hasta la fecha, Si no existe registro en algún mes los tomará como 0. El reporte guardará la fecha en la que genera el reporte.</p>
</li>
<li><p>Tras seleccionar el año, puede oprimir el botón de &quot;GENERAR REPORTE&quot; y cargará la vista siguiente donde se muestran tablas que incluye el reporte.</p>
</li>
<li><p>Puede generar un Documento de Formato Portable (PDF) para descargar o imprimir usando el botón con el icono de imprimir en la parte superior de lado derecho.</p>
</li>
</ol>
</div>


















<!-- Agradecimientos-->
<div class="scrollspy" id="agradecimientos">
<h1>Agradecimientos.</h1>
<p class="flow-text">Por el autor del proyecto:  Ernesto Valentin Caamal Peech.</p>
<p>
  Agradezco a la Institución Comisión Nacional de Áreas Naturales Protegidas por
  permitir realizar mi estancia en las oficinas de Felipe Carrillo Puerto, así como a
  los trabajadores de la dependencia que me brindaron su mano y su apoyo. <br>
  Al Lic. Juan Francisco Pat Cituc por el trato amable y la coordinación en conjunto
  para realizar de la mejor forma el proyecto. <br>
  Al Ing. Joaquín Díaz Quijano por el apoyo institucional en decisiones
  para llevar a cabo el proyecto y la estancia profesional. <br>
  A la Biol. Yadira por motivar de forma involuntaria a la creatividad del proyecto. <br>
  Al Biol. Omar Ortíz por la valiosa información acerca del Complejo Sian Ka'an. <br>
  A mi compañera Maritza Guadalupe por el apoyo incondicional en la vida personal y profesional. <br>
  A mis padres por el apoyo académico de por vida y todos aquellos innombrables por haber hecho posible
  la Plataforma Visitación con éxito rotundo.

</p>
</div>


  </div>
</div>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript">
$(document).ready(function() {
  $('.scrollspy').scrollSpy();
  console.log("Document ready.");
});

</script>
</html>
